package org.testCases;

import org.configurations.Helper;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

public class ValidateGetRequestForPeopleEndpoint extends Helper {

    @DataProvider(name = "peopleIds")
    private Object[][] validPeopleIds() {
        Object[] people2 = property.getStringArray("peopleId.2");
        Object[] people3 = property.getStringArray("peopleId.3");
        Object[] people9 = property.getStringArray("peopleId.9");
        return new Object[][]{
                people2, people3, people9
        };
    }

    @Test(dataProvider = "peopleIds")
    static void getPeople_validateStatus_and_NameValue(String ids, String nameExpected) {
        getPeople(ids).then().assertThat()
                .statusCode(200)
                .body("name", is(nameExpected));
    }

    @Test()
    static void getPeople_validate_height_mass_hairColor_type() {
        getPeople("1").then().assertThat()
                .body("height", instanceOf(String.class))
                .body("mass", instanceOf(String.class))
                .body("hair_color", instanceOf(String.class));
    }


}
