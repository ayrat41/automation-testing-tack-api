package org.testCases;

import org.configurations.Helper;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.is;

public class ValidateGetRequestForStarShipEndpoint extends Helper {

    @DataProvider(name = "starShipIds")
    private Object[][] validStarShipIds() {
        return new Object[][]{
                {"27", "Calamari Cruiser"}, {"10", "Millennium Falcon"}, {"9", "Death Star"}
        };
    }

    @Test(dataProvider = "starShipIds")
    static void getStarShip_validateStatus_and_NameValue(String ids, String nameExpected) {
        getStarShips(ids).then().assertThat()
                .statusCode(200)
                .body("name", is(nameExpected));
    }


}
