package org.testCases;

import org.configurations.Helper;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.is;

public class ValidateGetRequestForPlanetEndpoint extends Helper {

    @DataProvider(name = "planetIds")
    private Object[][] validPlanetIds() {
        Object[] planet2 = property.getStringArray("planetId.2");
        Object[] planet5 = property.getStringArray("planetId.5");
        Object[] planet7 = property.getStringArray("planetId.7");
        return new Object[][]{
                planet2,planet5,planet7
        };
    }

    @Test(dataProvider = "planetIds")
    static void getPlanet_validateStatus_and_NameValue(String ids, String nameExpected) {
        getPlanet(ids).then().assertThat()
                .statusCode(200)
                .body("name", is(nameExpected));
    }


}
