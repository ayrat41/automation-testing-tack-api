package org.configurations;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;

public abstract class TestBase {

    public static CompositeConfiguration readProperty(){
        CompositeConfiguration property = new CompositeConfiguration();
        try{
            property.addConfiguration(new PropertiesConfiguration("src/main/resources/endpoints.properties"));
            property.addConfiguration(new PropertiesConfiguration("src/main/resources/property.properties"));
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return property;
    }

}
