package org.configurations;

import io.restassured.response.Response;
import org.apache.commons.configuration.CompositeConfiguration;
import static io.restassured.RestAssured.get;

public class Helper extends TestBase{

    public static CompositeConfiguration property = readProperty();
    public static String url = property.getString("url");
    public static String planetsEndPoint = property.getString("planetsEndPoint");
    public static String starShipsEndPoint = property.getString("starShipsEndPoint");
    public static String peopleEndPoint = property.getString("peopleEndPoint");


    public static Response getPeople(String peopleId){
        Response response = get(url + peopleEndPoint + peopleId);
        return response;
    }

    public static Response getStarShips(String starShipsId){
        Response response = get(url + starShipsEndPoint + starShipsId);
        return response;
    }

    public static Response getPlanet(String planetId){
        Response response = get(url + planetsEndPoint + planetId);
        return response;
    }


}
